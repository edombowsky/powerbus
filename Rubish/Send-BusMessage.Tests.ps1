﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path).Replace(".Tests.", ".")
. "$here\$sut"

function Send-APIBusMessage {}

Describe "Send-BusMessage" {
    Context "Parameter validation" {
        It "ConnectionString null should throw" {
            { Send-BusMessage -QueueNameString "dummy" } | Should throw
        }

        It "QueueNameString null should throw" {
            { Send-BusMessage -ConnectionString "something" } | Should throw
        }

        It "All null should trow" {
            { Send-BusMessage } | Should throw
        }
    }
    $param = @{
        ConnectionString = "Endpoint=sb://spvtest.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=mbp72DG/CGwZSBp88e4uJDee6We6dVu2ZKM1zxkK921="
        QueueNameString = "dummy"           
    }

    Context "Mock Send-APIBusMessage" {
        Mock Send-APIBusMessage { $false }       
            It "failed to send should be false" {                           
                Send-BusMessage @param | Should be $false
            }

            It "failed to send should not throw " {                           
                { Send-BusMessage @param -Label "test" } | Should not throw
            }

        Mock Send-APIBusMessage { $true }
            It "Send success should be true" {                           
                Send-BusMessage @param -Label "test" | Should be $true
            }

            It "Send success should not throw " {                           
                { Send-BusMessage @param -Label "test" } | Should not throw
            }
    }
}
