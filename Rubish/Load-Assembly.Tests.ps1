﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path).Replace(".Tests.", ".")
. "$here\$sut"

Describe "Load-Assembly" {    
    Context "files exists" {
        It "Microsoft.ServiceBus.dll" {
            get-item -Path ".\Microsoft.ServiceBus.dll" | Should Exist
        }

        It "Microsoft.WindowsAzure.Configuration.dll" {
            get-item -Path ".\Microsoft.WindowsAzure.Configuration.dll" | Should Exist
        }
        
    }
    Context "load assembly that" {
        #Mock Test-Path {$true}
        It "exist should not trow" {
            { Load-Assembly -path ".\Microsoft.WindowsAzure.Configuration.dll" } | Should Not Throw
        }

        #Mock Test-Path {$false}
        It "is absent should throw" {
            { Load-Assembly -path ".\dummy.dll" } | Should Throw
        }
    }
}
