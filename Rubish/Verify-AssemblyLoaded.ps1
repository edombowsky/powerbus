﻿
function Verify-AssemblyLoaded {
[cmdletbinding()]
[outputtype([bool])]
Param(
    [ValidateNotNullorEmpty()]
    [validateNotNull()]
    [string] $AssemblyPath
)
    $f = $MyInvocation.MyCommand.Name
    Write-Verbose -Message "$f - Start"

    $Assembly = $null

    [bool]$Return = $null

    foreach($assembly in $AssemblyPath)
    {
        Write-Verbose -Message "$f -  Processing assemly $Assembly"
        $path = $Assembly | Resolve-Path
        $Directory = $path.path

        Write-Verbose -Message "$f -  Path is $($path.path)"

        $Directory = $Directory | Split-Path
        $assemblyName = $path.path.replace("$Directory\","").replace(".dll","")

        Write-Verbose "$f -  Checking if assembly is loaded"

        if(Get-Assembly $assemblyName)
        {
            Write-Verbose -Message "$f -  $assemblyName.dll is loaded"
            #Load-Assembly -Path $path.path
            $Return = $true        
        }
        else
        {
            Write-Verbose -Message "$f -  $assemblyName.dll is NOT loaded"
            $Return = $false
        }     
    }
    
    Write-Verbose -Message "$f - End"
    $Return
}

